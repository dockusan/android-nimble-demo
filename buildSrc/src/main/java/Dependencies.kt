object Versions {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29

    const val recyclerView = "1.1.0"
    const val constraintLayoutVersion = "1.1.3"
    const val archLifeCycleVersion = "2.0.0"
    const val annotationsVersion = "1.0.0"
    const val cardViewVersion = "1.0.0"
    const val googleMaterialDesignVersion = "1.3.0"
    const val roomVersion = "2.2.0-rc01"
    const val androidXSecurityVersion = "1.1.0-alpha03"

    const val daggerVersion = "2.22.1"
    const val androidDaggerVersion = "2.22.1"

    const val glideVersion = "4.9.0"
    const val retrofitVersion = "2.9.0"
    const val gsonVersion = "2.8.5"
    const val okhttp3Version = "4.4.1"

    const val imageCropperVersion = "2.4.7"
    const val lottieVersion = "3.0.0"
    const val pageIndicatorVersion = "1.0.6"
    const val kotlinVersion = "1.4.31"
    const val pdfViewerVersion = "2.8.2"
    const val flexboxVersion = "2.0.1"
    const val fbShimmerVersion = "0.5.0"
    const val timberVersion = "4.7.1"

    //Testing
    const val archTestCoreVersion = "2.0.0"
    const val mockitoKotlinVersion = "2.1.0"
    const val mockitoInlineVersion = "2.13.0"
    const val robolectricVersion = "4.3"
    const val testCoreKtxVersion = "1.2.0"
    const val mockkVersion = "1.9.3"
    const val junitVersion = "1.1.1"
    const val testVersion = "1.1.0"

    const val rxJava3Version = "3.0.0"
    const val rxAndroid3Version = "3.0.0"
}

object Libraries {
    const val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    const val gson = "com.google.code.gson:gson:${Versions.gsonVersion}"
    const val retrofit2 = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    const val retrofit2Adapter = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofitVersion}"
    const val retrofit2Gson = "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    const val okHttp3LoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp3Version}"
    const val okHttp3 = "com.squareup.okhttp3:okhttp:${Versions.okhttp3Version}"
    const val imageCropper = "com.theartofdev.edmodo:android-image-cropper:${Versions.imageCropperVersion}"
    const val lottie = "com.airbnb.android:lottie:${Versions.lottieVersion}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val lifeCycleExtension = "androidx.lifecycle:lifecycle-extensions:${Versions.archLifeCycleVersion}"
    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinVersion}"
    const val pageIndicator = "ru.tinkoff.scrollingpagerindicator:scrollingpagerindicator:${Versions.pageIndicatorVersion}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.roomVersion}"
    const val androidXSecurity = "androidx.security:security-crypto:${Versions.androidXSecurityVersion}"
    const val roomRuntime = "androidx.room:room-runtime:${Versions.roomVersion}"
    const val roomRxJava3 = "androidx.room:room-rxjava3:${Versions.roomVersion}"
    const val rxAndroid3 = "io.reactivex.rxjava3:rxandroid:${Versions.rxAndroid3Version}"
    const val rxJava3 = "io.reactivex.rxjava3:rxjava:${Versions.rxJava3Version}"
    const val pdfViewer = "com.github.barteksc:android-pdf-viewer:${Versions.pdfViewerVersion}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timberVersion}"
    const val flexboxLayout = "com.google.android:flexbox:${Versions.flexboxVersion}"
    const val fbShimmer = "com.facebook.shimmer:shimmer:${Versions.fbShimmerVersion}"

    //SupportLibraries
    const val androidXannotations = "androidx.annotation:annotation:${Versions.annotationsVersion}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardViewVersion}"
    const val design = "com.google.android.material:material:${Versions.googleMaterialDesignVersion}"

    //DaggerLibraries
    const val dagger2 = "com.google.dagger:dagger:${Versions.daggerVersion}"
    const val daggerCompiler2 = "com.google.dagger:dagger-compiler:${Versions.daggerVersion}"
    const val androidDagger2 = "com.google.dagger:dagger-android-support:${Versions.androidDaggerVersion}"
    const val androidDaggerCompiler2 = "com.google.dagger:dagger-android-processor:${Versions.androidDaggerVersion}"
}

object TestLibraries {
    const val archTestCore = "androidx.arch.core:core-testing:${Versions.archTestCoreVersion}"
    const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlinVersion}"
    const val jsonTest = "org.json:json:20180813"
    const val mockitoInline = "org.mockito:mockito-inline:${Versions.mockitoInlineVersion}"
    const val robolectric = "org.robolectric:robolectric:${Versions.robolectricVersion}"
    const val testCoreKtx = "androidx.test:core:${Versions.testCoreKtxVersion}"
    const val mockk = "io.mockk:mockk:${Versions.mockkVersion}"
    const val mockkAndroid = "io.mockk:mockk-android:${Versions.mockkVersion}"
    const val junit = "androidx.test.ext:junit:${Versions.junitVersion}"
    const val testRunner = "androidx.test:runner:${Versions.testVersion}"
    const val testRule = "androidx.test:rules:${Versions.testVersion}"
}
