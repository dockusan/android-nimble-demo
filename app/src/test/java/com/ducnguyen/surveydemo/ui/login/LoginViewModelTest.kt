package com.ducnguyen.surveydemo.ui.login

import com.ducnguyen.surveydemo.ArchViewModelTestBase
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.extensions.assertValue
import com.ducnguyen.surveydemo.extensions.should_be
import com.ducnguyen.surveydemo.repositories.LoginRepository
import com.ducnguyen.surveydemo.repositories.ResourceRepository
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Before
import org.junit.Test

class LoginViewModelTest : ArchViewModelTestBase() {
    private val mockLoginRepo: LoginRepository = mockk(relaxed = true)
    private val mockResourceRepo: ResourceRepository = mockk(relaxed = true)

    private lateinit var sut: LoginViewModel

    @Before
    fun setUp() {
        sut = LoginViewModel(mockLoginRepo, mockResourceRepo)
    }

    @Test
    fun `test user login success`() {
        //Given
        val dummyEmail = "a@123"
        val dummyPassword = "1234"
        every { mockLoginRepo.login(any(), any()) } returns Single.just(true)

        //When
        sut.login(dummyEmail, dummyPassword)

        //Then
        verify {
            mockLoginRepo.login(
                match { it == dummyEmail },
                match { it == dummyPassword }
            )
        }
        sut.loginLiveData.value should_be ApiResult.Success(true)
    }

    @Test
    fun `test user email empty`() {
        //Given
        val dummyEmail = ""
        val dummyPassword = "1234"
        val dummyString = "Email empty"
        every { mockResourceRepo.emptyEmail } returns dummyString

        //When
        sut.login(dummyEmail, dummyPassword)

        //Then
        sut.loginLiveData.value.assertValue {
            this is ApiResult.Error && this.exception.message == dummyString
        }
    }

    @Test
    fun `test user password empty`() {
        //Given
        val dummyEmail = "abc"
        val dummyPassword = ""
        val dummyString = "Password empty"
        every { mockResourceRepo.emptyPassword } returns dummyString

        //When
        sut.login(dummyEmail, dummyPassword)

        //Then
        sut.loginLiveData.value.assertValue {
            this is ApiResult.Error && this.exception.message == dummyString
        }
    }

    @Test
    fun `test user login fail`() {
        //Given
        val dummyEmail = "a@123"
        val dummyPassword = "1234"
        val dummyString = "dummy"
        val dummyException = Exception("dummy")
        every { mockLoginRepo.login(any(), any()) } returns Single.error(dummyException)
        every { mockResourceRepo.invalidUserNameOrPassword } returns dummyString

        //When
        sut.login(dummyEmail, dummyPassword)

        //Then
        verify {
            mockLoginRepo.login(
                match { it == dummyEmail },
                match { it == dummyPassword }
            )
        }

        sut.loginLiveData.value.assertValue {
            this is ApiResult.Error && this.exception.message == dummyString
        }
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }
}