package com.ducnguyen.surveydemo.ui.main

import com.ducnguyen.surveydemo.ArchViewModelTestBase
import com.ducnguyen.surveydemo.domain.models.ScreenName
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.domain.models.survey.SurveyUIModel
import com.ducnguyen.surveydemo.extensions.should_be
import com.ducnguyen.surveydemo.repositories.SurveyRepository
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainViewModelTest : ArchViewModelTestBase() {
    private val mockSurveyRepo: SurveyRepository = mockk(relaxed = true)
    private lateinit var sut: MainViewModel

    @Before
    fun setUp() {
        sut = MainViewModel(mockSurveyRepo)
    }

    @Test
    fun `test fetch survey success`() {
        //Given
        val resultsMock = mockk<List<SurveyUIModel>>().apply {
            every { size } returns 3
        }
        every { mockSurveyRepo.fetchSurveys(any(), any()) } returns Single.just(resultsMock)
        //When
        sut.fetchSurveys(1, 3)
        //Then
        sut.surveyLiveData.value should_be ApiResult.Success(resultsMock)
    }

    @Test
    fun `test fetch survey failure`() {
        //Given
        val exception = Exception("dummy")
        every { mockSurveyRepo.fetchSurveys(any(), any()) } returns Single.error(exception)
        //When
        sut.fetchSurveys(1, 3)
        //Then
        sut.surveyLiveData.value should_be ApiResult.Error(exception)
    }

    @Test
    fun `test go to journey preview`() {
        //Given
        sut.isPreviewOpen = false
        //When
        sut.gotoPreviewOrDetail()
        //Then
        sut.openScreenLiveData.value should_be ScreenName.SURVEY_PREVIEW
    }

    @Test
    fun `test go to journey detail`() {
        //Given
        sut.isPreviewOpen = true
        //When
        sut.gotoPreviewOrDetail()
        //Then
        sut.openScreenLiveData.value should_be ScreenName.SURVEY_DETAIL
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }
}