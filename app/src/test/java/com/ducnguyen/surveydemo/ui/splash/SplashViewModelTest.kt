package com.ducnguyen.surveydemo.ui.splash

import com.ducnguyen.surveydemo.ArchViewModelTestBase
import com.ducnguyen.surveydemo.extensions.should_be
import com.ducnguyen.surveydemo.repositories.PreferenceRepository
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Test

class SplashViewModelTest : ArchViewModelTestBase() {
    private val mockPrefRepo: PreferenceRepository = mockk()
    private lateinit var sut: SplashViewModel

    @Before
    fun setUp() {
        sut = SplashViewModel(mockPrefRepo)
    }

    @Test
    fun `test user is registered`() {
        val dummyRefreshToken = "dummy_refresh_token"
        every { mockPrefRepo.refreshToken } returns dummyRefreshToken
        sut.isRegistered() should_be true
    }

    @Test
    fun `test user is not registered`() {
        every { mockPrefRepo.refreshToken } returns null
        sut.isRegistered() should_be false
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }
}