package com.ducnguyen.surveydemo.extensions

import junit.framework.TestCase.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue

infix fun <T> T.should_be(expected: T) {
    assertEquals(expected, this)
}

infix fun <T> T.should_not_be(expected: T) {
    assertNotEquals(expected, this)
}

fun <T> T.assertValue(predicate: T.() -> Boolean) {
    assertTrue(predicate(this))
}