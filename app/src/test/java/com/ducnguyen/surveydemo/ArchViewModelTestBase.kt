package com.ducnguyen.surveydemo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule

open class ArchViewModelTestBase {

    @Rule
    @JvmField
    val rxRule = RxImmediateSchedulerRule()

    @Rule
    @JvmField
    val archRule = InstantTaskExecutorRule()
}