package com.ducnguyen.surveydemo.service

import com.ducnguyen.surveydemo.domain.api.LoginApi
import com.ducnguyen.surveydemo.domain.models.network.TokenBodyRequest
import com.ducnguyen.surveydemo.repositories.PreferenceRepository
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    private val loginApi: LoginApi,
    private val pref: PreferenceRepository
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        /**
        Whenever api call get 401,
        authenticator take responsibility to call refresh token api to get new token,
        then call api again
         **/

        val refreshToken = pref.refreshToken
        if (refreshToken.isNullOrEmpty()) {
            return null
        }
        val authTokenResponse = loginApi.refreshToken(TokenBodyRequest(refreshToken))
            .blockingGet()
        if (authTokenResponse.responseData != null) {
            authTokenResponse.responseData.attributes.let {

                val tokenString = "${it.tokenType} ${it.accessToken}"
                pref.updateAuthToken(it.refreshToken, tokenString)

                return response.request.newBuilder()
                    .header(AUTHORIZATION, tokenString)
                    .build()
            }
        } else {
            //TODO refresh token expired, return HTTP 400 and navigate user to login screen
            return null
        }
    }
}