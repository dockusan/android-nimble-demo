package com.ducnguyen.surveydemo.service

import com.ducnguyen.surveydemo.repositories.PreferenceRepository
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

const val AUTHORIZATION = "Authorization"

class AuthenticateInterceptor @Inject constructor(
    private val prefRepo: PreferenceRepository
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .addHeader(AUTHORIZATION, "${prefRepo.requestToken}")
            .build()
        return chain.proceed(newRequest)
    }
}