package com.ducnguyen.surveydemo.di

import com.ducnguyen.surveydemo.BuildConfig
import com.ducnguyen.surveydemo.domain.api.LoginApi
import com.ducnguyen.surveydemo.domain.api.SurveyApi
import com.ducnguyen.surveydemo.service.AuthRetrofit
import com.ducnguyen.surveydemo.service.AuthenticateInterceptor
import com.ducnguyen.surveydemo.service.PlainRetrofit
import com.ducnguyen.surveydemo.service.TokenAuthenticator
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ServiceModule {

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun provideOkHttp(): OkHttpClient {
        val okHttpBuilder = OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
            okHttpBuilder.addInterceptor(logging)
        }
        return okHttpBuilder.build()
    }

    @Singleton
    @Provides
    @PlainRetrofit
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Singleton
    @Provides
    @AuthRetrofit
    fun provideAuthRetrofit(
        gson: Gson,
        okHttpClient: OkHttpClient,
        tokenAuthenticator: TokenAuthenticator,
        authInterceptor: AuthenticateInterceptor
    ): Retrofit {
        val authOkHttpClient = okHttpClient.newBuilder()
            .authenticator(tokenAuthenticator)
            .addInterceptor(authInterceptor)
            .build()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(authOkHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Singleton
    @Provides
    fun provideLoginService(@PlainRetrofit retrofit: Retrofit): LoginApi {
        return retrofit.create(LoginApi::class.java)
    }

    @Singleton
    @Provides
    fun provideSurveyService(@AuthRetrofit retrofit: Retrofit): SurveyApi {
        return retrofit.create(SurveyApi::class.java)
    }
}
