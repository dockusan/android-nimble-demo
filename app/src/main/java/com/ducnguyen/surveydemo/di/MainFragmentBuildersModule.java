package com.ducnguyen.surveydemo.di;

import com.ducnguyen.surveydemo.ui.main.PreviewSurveyFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuildersModule {
    
    @ContributesAndroidInjector
    abstract PreviewSurveyFragment contributePreviewSurveyFragment();
}