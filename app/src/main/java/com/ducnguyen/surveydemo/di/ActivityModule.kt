package com.ducnguyen.surveydemo.di

import com.ducnguyen.surveydemo.ui.login.LoginActivity
import com.ducnguyen.surveydemo.ui.main.MainActivity
import com.ducnguyen.surveydemo.ui.splash.SplashActivity
import com.ducnguyen.surveydemo.ui.survey.SurveyDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeSurveyDetailActivity(): SurveyDetailActivity
}
