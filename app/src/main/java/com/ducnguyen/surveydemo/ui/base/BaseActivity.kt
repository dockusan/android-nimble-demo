package com.ducnguyen.surveydemo.ui.base

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.ducnguyen.surveydemo.utils.applyFullscreenTheme
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

abstract class BaseActivity<B : ViewBinding> : DaggerAppCompatActivity() {

    @Inject
    internal lateinit var vmFactory: ViewModelProvider.Factory

    private val compositeDisposable = CompositeDisposable()
    lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyFullscreenTheme()
        binding = getViewBinding()
        setContentView(binding.root)
        subscribeVM()
        initUI()
    }

    abstract fun getViewBinding(): B

    open fun subscribeVM() {}

    open fun initUI() {}

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun addToDispose(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }
}
