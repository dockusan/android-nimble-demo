package com.ducnguyen.surveydemo.ui.base

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()