package com.ducnguyen.surveydemo.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ducnguyen.surveydemo.databinding.ItemSurveyListBinding
import com.ducnguyen.surveydemo.domain.models.survey.SurveyUIModel

class SurveyAdapter : ListAdapter<SurveyUIModel, SurveyAdapter.SurveyViewHolder>(DIFF_CALLBACK) {
    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<SurveyUIModel>() {
            override fun areItemsTheSame(oldItem: SurveyUIModel, newItem: SurveyUIModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: SurveyUIModel,
                newItem: SurveyUIModel
            ): Boolean {
                return oldItem.title == newItem.title
                        && oldItem.description == newItem.description
                        && oldItem.backgroundUrl == newItem.backgroundUrl
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SurveyViewHolder {
        val view = ItemSurveyListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SurveyViewHolder(view)
    }

    override fun onBindViewHolder(holder: SurveyViewHolder, position: Int) {
        holder.config(getItem(position))
    }

    inner class SurveyViewHolder(private val binding: ItemSurveyListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun config(item: SurveyUIModel) {
            with(binding) {
                tvTitle.text = item.title
                tvDescription.text = item.description
                Glide.with(root.context)
                    .load(item.backgroundUrl)
                    .override(300)
                    .into(ivBackground)
            }
        }
    }
}