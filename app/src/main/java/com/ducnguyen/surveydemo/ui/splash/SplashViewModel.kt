package com.ducnguyen.surveydemo.ui.splash

import com.ducnguyen.surveydemo.repositories.PreferenceRepository
import com.ducnguyen.surveydemo.ui.base.BaseViewModel
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val prefRepo: PreferenceRepository
) : BaseViewModel() {

    fun isRegistered(): Boolean {
        return prefRepo.refreshToken != null
    }
}
