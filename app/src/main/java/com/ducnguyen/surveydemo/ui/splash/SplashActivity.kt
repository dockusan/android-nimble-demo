package com.ducnguyen.surveydemo.ui.splash

import android.content.Intent
import android.os.Bundle
import com.ducnguyen.surveydemo.databinding.ActivitySplashBinding
import com.ducnguyen.surveydemo.ui.base.BaseActivity
import com.ducnguyen.surveydemo.ui.login.LoginActivity
import com.ducnguyen.surveydemo.ui.main.MainActivity
import com.ducnguyen.surveydemo.utils.delayRxAction
import com.ducnguyen.surveydemo.utils.viewModelProvider
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    private val viewModel: SplashViewModel by viewModelProvider { vmFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        delayRxAction(2, TimeUnit.SECONDS) {
            gotoNextScreen()
        }.let { addToDispose(it) }
    }

    override fun getViewBinding(): ActivitySplashBinding {
        return ActivitySplashBinding.inflate(layoutInflater)
    }

    private fun gotoNextScreen() {
        if (viewModel.isRegistered()) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
            overridePendingTransition(0, 0)
        }
        finish()
    }
}