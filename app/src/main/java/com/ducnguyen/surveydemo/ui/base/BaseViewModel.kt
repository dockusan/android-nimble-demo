package com.ducnguyen.surveydemo.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

open class BaseViewModel : ViewModel() {
    private val disposeOnClear = CompositeDisposable()

    fun Disposable.untilCleared() = this.also {
        disposeOnClear.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        disposeOnClear.clear()
    }
}
