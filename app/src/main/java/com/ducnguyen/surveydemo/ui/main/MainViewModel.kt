package com.ducnguyen.surveydemo.ui.main

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import com.ducnguyen.surveydemo.domain.models.ScreenName
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.domain.models.survey.SurveyUIModel
import com.ducnguyen.surveydemo.repositories.SurveyRepository
import com.ducnguyen.surveydemo.ui.base.BaseViewModel
import com.ducnguyen.surveydemo.utils.SingleLiveEvent
import com.ducnguyen.surveydemo.utils.applySingleSchedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val surveyRepository: SurveyRepository
) : BaseViewModel() {
    val surveyLiveData = MutableLiveData<ApiResult<List<SurveyUIModel>>>()

    @VisibleForTesting
    var isPreviewOpen = false

    val openScreenLiveData = SingleLiveEvent<ScreenName>()

    fun fetchSurveys(pageNumber: Int, pageSize: Int) {
        surveyRepository.fetchSurveys(pageNumber, pageSize)
            .compose(applySingleSchedulers())
            .doOnSubscribe { surveyLiveData.value = ApiResult.Loading }
            .subscribe({
                surveyLiveData.value = ApiResult.Success(it)
            }, {
                surveyLiveData.value = ApiResult.Error(it)
            })
            .untilCleared()
    }

    fun gotoPreviewOrDetail() {
        if (!isPreviewOpen) {
            openScreenLiveData.value = ScreenName.SURVEY_PREVIEW
            isPreviewOpen = true
        } else {
            openScreenLiveData.value = ScreenName.SURVEY_DETAIL
        }
    }

    fun onPreviewClose() {
        isPreviewOpen = false
    }
}
