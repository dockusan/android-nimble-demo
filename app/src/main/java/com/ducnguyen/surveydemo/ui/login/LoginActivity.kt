package com.ducnguyen.surveydemo.ui.login

import android.app.AlertDialog
import android.content.Intent
import com.ducnguyen.surveydemo.R
import com.ducnguyen.surveydemo.databinding.ActivityLoginBinding
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.ui.base.BaseActivity
import com.ducnguyen.surveydemo.ui.main.MainActivity
import timber.log.Timber
import com.ducnguyen.surveydemo.utils.isVisible
import com.ducnguyen.surveydemo.utils.viewModelProvider

class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private val viewModel: LoginViewModel by viewModelProvider { vmFactory }

    override fun getViewBinding(): ActivityLoginBinding {
        return ActivityLoginBinding.inflate(layoutInflater)
    }

    override fun initUI() {
        binding.btnLogin.setOnClickListener {
            viewModel.login(
                binding.edtUsername.text.toString(),
                binding.edtPassword.text.toString()
            )
        }

        binding.tvForgot.setOnClickListener {
            Timber.d("Go to forgot screen")
        }
    }

    override fun subscribeVM() {
        viewModel.loginLiveData.observe(this) {
            binding.btnLogin.isVisible = true
            when (it) {
                is ApiResult.Success -> {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                is ApiResult.Error -> {
                    val builder = AlertDialog.Builder(this)
                    builder
                        .setTitle(R.string.all_error)
                        .setMessage(it.exception.localizedMessage)
                        .setPositiveButton(R.string.all_ok) { _, _ -> }
                    builder.create().show()
                }
                ApiResult.Loading -> {
                    binding.btnLogin.isVisible = false
                }
            }
        }
    }
}