package com.ducnguyen.surveydemo.ui.login

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.repositories.LoginRepository
import com.ducnguyen.surveydemo.repositories.ResourceRepository
import com.ducnguyen.surveydemo.ui.base.BaseViewModel
import com.ducnguyen.surveydemo.utils.applySingleSchedulers
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: LoginRepository,
    private val resource: ResourceRepository,
) : BaseViewModel() {

    val loginLiveData = MutableLiveData<ApiResult<Boolean>>()

    fun login(email: String, password: String) {
        if (!validateUserInput(email, password)) return

        repository.login(email, password)
            .compose(applySingleSchedulers())
            .doOnSubscribe { loginLiveData.value = ApiResult.Loading }
            .subscribe({
                loginLiveData.value = ApiResult.Success(true)
            }, {
                loginLiveData.value =
                    ApiResult.Error(Exception(resource.invalidUserNameOrPassword))
            })
            .untilCleared()
    }

    @VisibleForTesting
    fun validateUserInput(email: String, password: String): Boolean {
        if (email.isEmpty()) {
            loginLiveData.value = ApiResult.Error(Exception(resource.emptyEmail))
            return false
        }
        if (password.isEmpty()) {
            loginLiveData.value = ApiResult.Error(Exception(resource.emptyPassword))
            return false
        }
        //TODO more user input validate like valid email or strong password
        return true
    }
}