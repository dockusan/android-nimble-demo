package com.ducnguyen.surveydemo.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.ducnguyen.surveydemo.databinding.FragmentPreviewSurveyBinding
import com.ducnguyen.surveydemo.domain.models.survey.SurveyUIModel
import com.ducnguyen.surveydemo.ui.base.BaseFragment
import com.google.gson.Gson
import javax.inject.Inject

private const val KEY_SURVEY_ITEM = "KEY_SURVEY_ITEM"

class PreviewSurveyFragment : BaseFragment() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentPreviewSurveyBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
        initUI(binding)
        return binding.root
    }

    private fun initUI(binding: FragmentPreviewSurveyBinding) {
        val surveyString = arguments?.getString(KEY_SURVEY_ITEM) ?: ""
        if (surveyString.isNotEmpty()) {
            val survey = Gson().fromJson(surveyString, SurveyUIModel::class.java)
            Glide.with(requireActivity())
                .load(survey.backgroundUrl)
                .dontAnimate()
                .dontTransform()
                .into(binding.ivBackground)
            binding.tvTitle.text = survey.title
            binding.tvDescription.text = survey.description
        }
    }

    companion object {
        fun newInstance(survey: SurveyUIModel): PreviewSurveyFragment {
            return PreviewSurveyFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_SURVEY_ITEM, Gson().toJson(survey))
                }
            }
        }

        const val TAG = "PreviewSurveyFragment"
    }
}