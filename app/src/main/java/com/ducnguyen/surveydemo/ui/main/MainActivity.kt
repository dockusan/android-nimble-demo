package com.ducnguyen.surveydemo.ui.main

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.core.view.GravityCompat
import com.ducnguyen.surveydemo.R
import com.ducnguyen.surveydemo.databinding.ActivityMainBinding
import com.ducnguyen.surveydemo.domain.models.ScreenName
import com.ducnguyen.surveydemo.domain.models.network.ApiResult
import com.ducnguyen.surveydemo.ui.base.BaseActivity
import com.ducnguyen.surveydemo.ui.main.adapter.SurveyAdapter
import com.ducnguyen.surveydemo.ui.survey.SurveyDetailActivity
import com.ducnguyen.surveydemo.utils.isVisible
import com.ducnguyen.surveydemo.utils.viewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : BaseActivity<ActivityMainBinding>() {

    private lateinit var adapter: SurveyAdapter
    private val viewModel: MainViewModel by viewModelProvider { vmFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchSurveys(1, 3)
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.viewLoading.root.isVisible = isLoading
        if (isLoading) {
            binding.viewLoading.sflLoading.startShimmer()
        } else {
            binding.viewLoading.sflLoading.stopShimmer()
        }
    }

    override fun subscribeVM() {
        viewModel.surveyLiveData.observe(this) {
            binding.fabPreview.isVisible = it is ApiResult.Success
            showLoading(it is ApiResult.Loading)
            when (it) {
                is ApiResult.Success -> {
                    adapter.submitList(it.data)
                }
                is ApiResult.Error -> {
                    val builder = AlertDialog.Builder(this)
                    builder
                        .setTitle(R.string.all_error)
                        .setMessage(it.exception.localizedMessage)
                        .setCancelable(false)
                        .setPositiveButton(R.string.all_ok) { _, _ -> }
                    builder.create().show()
                }
            }
        }

        viewModel.openScreenLiveData.observe(this) { screenName ->
            when (screenName) {
                ScreenName.SURVEY_PREVIEW -> openPreviewScreen()
                ScreenName.SURVEY_DETAIL -> openDetailScreen()
                else -> {
                }
            }
        }
    }

    private fun openPreviewScreen() {
        val currentItem = binding.vpSurvey.currentItem
        val surveyItem = adapter.currentList[currentItem]
        updatePreviewUI(true)
        //TODO handle android material cross fade in animation
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.fl_container,
                PreviewSurveyFragment.newInstance(surveyItem),
                PreviewSurveyFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }

    private fun openDetailScreen() {
        startActivity(Intent(this, SurveyDetailActivity::class.java))
    }

    override fun initUI() {
        adapter = SurveyAdapter()
        binding.vpSurvey.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.vpSurvey) { tab, position -> }.attach()

        val dateFormat = SimpleDateFormat("EEEE, MMMM d", Locale.getDefault())
        val date = dateFormat.format(Calendar.getInstance().time)
        binding.tvDate.text = date

        binding.ivAvatar.setOnClickListener {
            //Open right drawer
            binding.dlLayout.openDrawer(GravityCompat.END)
        }

        binding.ivArrow.setOnClickListener {
            onBackPressed()
        }
        binding.fabPreview.shrink()
        binding.fabPreview.setOnClickListener {
            viewModel.gotoPreviewOrDetail()
        }
    }

    private fun updatePreviewUI(isOpen: Boolean) {
        if (isOpen) {
            binding.fabPreview.extend()
        } else {
            binding.fabPreview.shrink()
        }

        binding.ivArrow.isVisible = isOpen
        binding.ivAvatar.isVisible = !isOpen
    }

    override fun onBackPressed() {
        if (supportFragmentManager.findFragmentByTag(PreviewSurveyFragment.TAG) != null) {
            Timber.d("OnClosingPreview")
            updatePreviewUI(false)
            viewModel.onPreviewClose()
        }
        super.onBackPressed()
    }
}