package com.ducnguyen.surveydemo.ui.survey

import com.ducnguyen.surveydemo.databinding.ActivitySurveyDetailBinding
import com.ducnguyen.surveydemo.ui.base.BaseActivity

class SurveyDetailActivity : BaseActivity<ActivitySurveyDetailBinding>() {
    override fun getViewBinding(): ActivitySurveyDetailBinding {
        return ActivitySurveyDetailBinding.inflate(layoutInflater)
    }
}