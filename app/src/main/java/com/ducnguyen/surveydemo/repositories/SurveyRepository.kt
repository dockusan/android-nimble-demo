package com.ducnguyen.surveydemo.repositories

import com.ducnguyen.surveydemo.domain.api.SurveyApi
import com.ducnguyen.surveydemo.domain.models.survey.SurveyUIModel
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SurveyRepository @Inject constructor(
    private val surveyApi: SurveyApi
) {

    fun fetchSurveys(pageNumber: Int, pageSize: Int): Single<List<SurveyUIModel>> {
        return surveyApi.fetchSurveys(pageNumber, pageSize)
            .map {
                it.response.map { data ->
                    SurveyUIModel(
                        data.attributes.title,
                        data.attributes.description,
                        data.attributes.coverImageUrl.plus("l")
                    )
                }
            }
    }
}