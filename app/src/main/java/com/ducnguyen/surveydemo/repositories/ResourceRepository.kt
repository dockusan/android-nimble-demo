package com.ducnguyen.surveydemo.repositories

import android.content.Context
import com.ducnguyen.surveydemo.R
import javax.inject.Inject

class ResourceRepository @Inject constructor(private val context: Context) {
    val emptyEmail: String
        get() = context.getString(R.string.login_error_email_empty)

    val emptyPassword: String
        get() = context.getString(R.string.login_error_password_empty)

    val invalidUserNameOrPassword: String
        get() = context.getString(R.string.login_error_description)
}