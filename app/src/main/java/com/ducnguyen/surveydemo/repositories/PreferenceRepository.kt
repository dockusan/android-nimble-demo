package com.ducnguyen.surveydemo.repositories

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import javax.inject.Inject

private const val KEY_REFRESH_TOKEN = "pref_key_refresh_token"
private const val KEY_REQUEST_TOKEN = "pref_key_request_token"

class PreferenceRepository @Inject constructor(context: Context) {

    private val masterKey = MasterKey.Builder(context)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

    private val sharedPreferences = EncryptedSharedPreferences.create(
        context,
        "secret_shared_prefs",
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    var refreshToken: String?
        get() = sharedPreferences.getString(KEY_REFRESH_TOKEN, null)
        set(token) = sharedPreferences.edit().putString(KEY_REFRESH_TOKEN, token).apply()

    var requestToken: String?
        get() = sharedPreferences.getString(KEY_REQUEST_TOKEN, null)
        set(token) = sharedPreferences.edit().putString(KEY_REQUEST_TOKEN, token).apply()

    fun updateAuthToken(refreshToken: String, requestToken: String) {
        this.refreshToken = refreshToken
        this.requestToken = requestToken
    }

    fun clear() {
        sharedPreferences.edit().clear().apply()
    }
}
