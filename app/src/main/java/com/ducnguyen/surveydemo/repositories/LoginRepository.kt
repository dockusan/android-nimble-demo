package com.ducnguyen.surveydemo.repositories

import com.ducnguyen.surveydemo.domain.api.LoginApi
import com.ducnguyen.surveydemo.domain.models.network.LoginBodyRequest
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val loginApi: LoginApi,
    private val prefRepo: PreferenceRepository
) {

    fun login(email: String, password: String): Single<Boolean> {
        return loginApi.login(LoginBodyRequest(email, password))
            .flatMap {
                it.responseData?.attributes?.let { attr ->
                    prefRepo.refreshToken = attr.refreshToken
                    prefRepo.requestToken = "${attr.tokenType} ${attr.accessToken}"
                }
                Single.just(it.responseData != null)
            }
    }
}