package com.ducnguyen.surveydemo.utils

import android.app.Activity
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

fun Activity.applyFullscreenTheme(darkStatusBar: Boolean = false) {
    clearStatusBarColor()
    window.decorView.systemUiVisibility =
        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    if (darkStatusBar) darkStatusBar() else lightStatusBar()
}

fun Activity.darkStatusBar() {
    if (window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR > 0) {
        return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        window.decorView.systemUiVisibility =
            window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}

fun Activity.lightStatusBar() {
    if (window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR <= 0) {
        return
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        window.decorView.systemUiVisibility =
            window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
    }
}

fun Activity.clearStatusBarColor() {
    changeStatusBarColorRes(android.R.color.transparent)
}

fun Activity.changeStatusBarColorRes(@ColorRes colorRes: Int) {
    this.changeStatusBarColor(ContextCompat.getColor(this@changeStatusBarColorRes, colorRes))
}

fun Activity.changeStatusBarColor(color: Int) {
    window.changeStatusBarColor(color)
}

fun Window.changeStatusBarColor(color: Int) {
    this.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    this.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    this.statusBarColor = color
}

inline fun <reified VM : ViewModel> FragmentActivity.viewModelProvider(crossinline component: () -> ViewModelProvider.Factory) =
    lazy {
        ViewModelProvider(this, component.invoke())[VM::class.java]
    }

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(crossinline component: () -> ViewModelProvider.Factory) =
    lazy {
        ViewModelProvider(this, component.invoke())[VM::class.java]
    }
