package com.ducnguyen.surveydemo.domain.models.network

sealed class ApiResult<out R> {
    data class Success<out T>(val data: T) : ApiResult<T>()

    data class Error(val exception: Throwable) : ApiResult<Nothing>()

    object Loading : ApiResult<Nothing>()
}

fun <T> ApiResult<T>.successOr(fallback: T): T {
    val asSuccess = this as? ApiResult.Success<T>
    return if (asSuccess != null) asSuccess.data else fallback
}

fun <T> ApiResult<T>.successOrNull(): T? {
    return (this as? ApiResult.Success<T>)?.data
}

fun <T> ApiResult<T>.failedOrNull(): Throwable? {
    return (this as? ApiResult.Error)?.exception
}