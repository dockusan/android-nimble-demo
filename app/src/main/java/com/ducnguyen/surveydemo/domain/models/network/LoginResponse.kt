package com.ducnguyen.surveydemo.domain.models.network

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("data")
    val responseData: LoginData?,
    @SerializedName("errors")
    val errors: List<LoginError>?
)

data class LoginData(
    @SerializedName("attributes")
    val attributes: Attributes,
    @SerializedName("id")
    val id: Int,
    @SerializedName("type")
    val type: String
)

data class Attributes(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("created_at")
    val createdAt: Int,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("token_type")
    val tokenType: String
)

data class LoginError(
    @SerializedName("code")
    val code: String,
    @SerializedName("detail")
    val detail: String,
    @SerializedName("source")
    val source: String
)