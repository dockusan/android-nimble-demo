package com.ducnguyen.surveydemo.domain.models.network

import com.ducnguyen.surveydemo.BuildConfig
import com.google.gson.annotations.SerializedName

open class BaseRequestBody(
    @SerializedName("client_id")
    private val clientId: String = BuildConfig.CLIENT_ID,
    @SerializedName("client_secret")
    private val clientSecret: String = BuildConfig.CLIENT_SECRET
)