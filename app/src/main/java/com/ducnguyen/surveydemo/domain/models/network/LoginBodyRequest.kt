package com.ducnguyen.surveydemo.domain.models.network

import com.google.gson.annotations.SerializedName

class LoginBodyRequest(
    @SerializedName("email")
    private val email: String,
    @SerializedName("password")
    private val password: String,
    @SerializedName("grant_type")
    private val grantType: String = "password",
) : BaseRequestBody()