package com.ducnguyen.surveydemo.domain.models

enum class ScreenName {
    SURVEY_PREVIEW, SURVEY_DETAIL
}