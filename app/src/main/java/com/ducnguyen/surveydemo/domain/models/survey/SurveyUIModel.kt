package com.ducnguyen.surveydemo.domain.models.survey

data class SurveyUIModel(
    val title: String,
    val description: String,
    val backgroundUrl: String
)