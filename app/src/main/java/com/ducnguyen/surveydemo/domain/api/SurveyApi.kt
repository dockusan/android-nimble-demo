package com.ducnguyen.surveydemo.domain.api

import com.ducnguyen.surveydemo.domain.models.survey.SurveyResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SurveyApi {
    @GET("api/v1/surveys")
    fun fetchSurveys(
        @Query("page[number]") pageNumber: Int,
        @Query("page[size]") pageSize: Int,
    ): Single<SurveyResponse>
}