package com.ducnguyen.surveydemo.domain.models.network

import com.google.gson.annotations.SerializedName

class TokenBodyRequest(
    @SerializedName("refresh_token")
    private val refreshToken: String,
    @SerializedName("grant_type")
    private val grantType: String = "refresh_token",
) : BaseRequestBody()