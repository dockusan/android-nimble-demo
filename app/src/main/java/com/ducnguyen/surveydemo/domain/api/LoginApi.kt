package com.ducnguyen.surveydemo.domain.api

import com.ducnguyen.surveydemo.domain.models.network.LoginBodyRequest
import com.ducnguyen.surveydemo.domain.models.network.LoginResponse
import com.ducnguyen.surveydemo.domain.models.network.TokenBodyRequest
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {
    @POST("api/v1/oauth/token")
    fun login(@Body request: LoginBodyRequest): Single<LoginResponse>

    @POST("api/v1/oauth/token")
    fun refreshToken(@Body request: TokenBodyRequest): Single<LoginResponse>
}